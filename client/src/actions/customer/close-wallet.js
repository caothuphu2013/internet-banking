import {
    FETCH_USER_WALLETS,
    FETCH_USER_WALLETS_SUCCESS,
    FETCH_USER_WALLETS_FAIL,
    CLOSE_USER_WALLET,
    CLOSE_USER_WALLET_SUCCESS,
    CLOSE_USER_WALLET_FAIL,
    RESET_STORE
} from '../../constants/customer/close-wallet';
import { URL_SERVER } from '../../configs/server';

const fetchUserWallets = (email, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_USER_WALLETS });
        fetch(`${URL_SERVER}/wallet/find?email=${email}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_USER_WALLETS_SUCCESS,
                    userWallets: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_USER_WALLETS_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const closeUserWallet = (email, walletNumber, accessToken) => {
    return (dispatch) => {
        dispatch({ type: CLOSE_USER_WALLET });

        fetch(`${URL_SERVER}/wallet/close`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                x_accesstoken: accessToken
            }),
            body: JSON.stringify({
                email,
                walletNumber
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: CLOSE_USER_WALLET_SUCCESS,
                    userWallets: res.data.data,
                    messageSuccess: res.data.message
                });
            }
            else {
                dispatch({
                    type: CLOSE_USER_WALLET_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const resetStore = () => {
    return (dispatch) => {
        dispatch({
            type: RESET_STORE,
        });
    }
}

export {
    fetchUserWallets,
    closeUserWallet,
    resetStore
}