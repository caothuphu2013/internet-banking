import {
    FETCH_RECIPIENTS,
    FETCH_RECIPIENTS_SUCCESS,
    FETCH_RECIPIENTS_FAIL,
    UPDATE_RECIPIENT,
    UPDATE_RECIPIENT_SUCCESS,
    UPDATE_RECIPIENT_FAIL,
    DELETE_RECIPIENT,
    DELETE_RECIPIENT_SUCCESS,
    DELETE_RECIPIENT_FAIL,
    RESET_STORE,
    ADD_RECIPIENT,
    ADD_RECIPIENT_SUCCESS,
    ADD_RECIPIENT_FAIL,
    TOGGLE_MODAL_ADD_RECIPIENT
} from '../../constants/customer/setup-recipient';
import { URL_SERVER } from '../../configs/server';

const fetchRecipients = (email, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_RECIPIENTS });

        fetch(`${URL_SERVER}/receiver?email=${email}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_RECIPIENTS_SUCCESS,
                    recipients: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_RECIPIENTS_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const updateRecipient = (email, walletNumber, remindName, accessToken) => {
    return (dispatch) => {
        dispatch({ type: UPDATE_RECIPIENT });

        fetch(`${URL_SERVER}/receiver/update`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                x_accesstoken: accessToken
            }),
            body: JSON.stringify({
                email,
                walletNumber,
                remindName
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: UPDATE_RECIPIENT_SUCCESS,
                    recipients: res.data.data,
                    messageSuccess: res.data.message
                });
            }
            else {
                dispatch({
                    type: UPDATE_RECIPIENT_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}


const deleteRecipient = (email, walletNumber, accessToken) => {
    return (dispatch) => {
        dispatch({ type: DELETE_RECIPIENT });

        fetch(`${URL_SERVER}/receiver/delete`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                x_accesstoken: accessToken
            }),
            body: JSON.stringify({
                email,
                walletNumber,
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: DELETE_RECIPIENT_SUCCESS,
                    recipients: res.data.data,
                    messageSuccess: res.data.message
                });
            }
            else {
                dispatch({
                    type: DELETE_RECIPIENT_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const addRecipient = (email, receiverWalletNumber, remindName, accessToken) => {
    return (dispatch) => {
        dispatch({
            type: ADD_RECIPIENT
        });

        fetch(`${URL_SERVER}/receiver/create`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                x_accesstoken: accessToken
            }),
            body: JSON.stringify({
                email,
                receiverWalletNumber,
                remindName
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: ADD_RECIPIENT_SUCCESS,
                    recipients: res.data.data,
                    messageSuccess: res.data.message
                });
            }
            else {
                dispatch({
                    type: ADD_RECIPIENT_FAIL,
                    messageError: res.messageError
                });
            }
        })
    }
}

const toggleModalAddRecipient = (isShowModalAddRecipient) => {
    return (dispatch) => {
        dispatch({
            type: TOGGLE_MODAL_ADD_RECIPIENT,
            isShowModalAddRecipient
        });
    }
}

const resetStore = () => {
    return (dispatch) => {
        dispatch({
            type: RESET_STORE,
        });
    }
}

export {
    fetchRecipients,
    updateRecipient,
    deleteRecipient,
    addRecipient,
    toggleModalAddRecipient,
    resetStore
}