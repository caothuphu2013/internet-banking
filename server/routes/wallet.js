const express = require('express');
const {
  walletModel
} = require('../models/index');
const {
  rechargeWallet
} = require('../function/function');
const {
  handleSuccess,
  handleError
} = require('../helpers/request');

const {
  verifyToken
} = require('../helpers/auth');

let walletRoutes = express.Router();

walletRoutes.get('/find', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];

  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === req.query.email && payload.role === 'customer') {
        walletModel.getAllWalletsBalanceOfUser(req.query.email)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

walletRoutes.get('/get', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];

  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.role === 'customer') {
        walletModel.getWalletBalance(req.query.walletNumber)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

walletRoutes.post('/create', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];

  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.role === 'staff') {
        walletModel.createWallet(req.body.email)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

walletRoutes.post('/close', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];

  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === req.body.email && payload.role === 'customer') {
        walletModel.closeWallet(req.body.email, req.body.walletNumber)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

walletRoutes.post('/recharge', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];

  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.role === 'staff') {
        rechargeWallet(req.body.walletNumber, req.body.amount)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

walletRoutes.get('/track', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.role === 'customer') {
        walletModel.getUserInfoByWallet(req.query.walletNumber)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

module.exports = walletRoutes;