import {
    FETCH_PAYMENT_ACCOUNTS,
    FETCH_PAYMENT_ACCOUNTS_SUCCESS,
    FETCH_PAYMENT_ACCOUNTS_FAIL,
    TOGGLE_MODAL,
    RESET_STORE
} from '../../constants/customer/payment-accounts';
import { URL_SERVER } from '../../configs/server';

const fetchPaymentAccounts = (email, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_PAYMENT_ACCOUNTS });

        fetch(`${URL_SERVER}/wallet/find?email=${email}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_PAYMENT_ACCOUNTS_SUCCESS,
                    paymentAccounts: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_PAYMENT_ACCOUNTS_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const resetStore = () => {
    return dispatch => {
        dispatch({
            type: RESET_STORE
        })
    }
}

const toggleModal = (isShowModal) => {
    return (dispatch) => {
        dispatch({
            type: TOGGLE_MODAL,
            isShowModal
        });
    }
}

export {
    fetchPaymentAccounts,
    resetStore,
    toggleModal
}