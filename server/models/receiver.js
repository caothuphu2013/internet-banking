const q = require('q');
const {
  collections
} = require('../configs/db');
const {
  dbController
} = require('../controllers/index');

const createReceiver = (email, receiverWalletNumber, remindName) => {
  const d = q.defer();

  dbController.find(collections.user, {
      email
    })
    .then(result => {
      if (result.length > 0) {
        dbController.find(collections.receiver, {
            email,
            walletNumber: receiverWalletNumber
          })
          .then(res => {
            if (res.length != 0) {
              d.reject({
                status: 400,
                message: `dublicate record`
              })
              return d.promise;
            }
            dbController.find(collections.wallet, {
                walletNumber: receiverWalletNumber
              })
              .then(res => {
                if (res.length > 0) {
                  dbController.find(collections.user, {
                      email: res[0].email
                    })
                    .then(resu => {
                      if (resu.length > 0) {
                        if (remindName === undefined || remindName === null || remindName === "") {
                        remindName = resu[0].fullName;
                        }
                        dbController.insert(collections.receiver, {
                            email,
                            remindName,
                            walletNumber: receiverWalletNumber
                          })
                          .then(resul => {
                            dbController.find(collections.receiver, {
                              email
                            })
                            .then(data => {
                              d.resolve({
                                data,
                                message: `Create receiver record successfully!`
                              });
                            })
                          })
                          .catch(error => {
                            console.log(`error when creating receiver: ${error}`);
                            d.reject({
                              status: 500,
                              message: `server's error`
                            })
                          })
                      }
                    })
                } else {
                  d.reject({
                    status: 400,
                    message: `receiver wallet number is invalid`
                  })
                  return d.promise;
                }
              })
          })
          .catch(error => {
            console.log(`error when finding receiver: ${error}`);
            d.reject({
              status: 500,
              message: `server's error`
            })
          })
      }
    })
  return d.promise;
}

const getReceiversOfUser = (email) => {
  const d = q.defer();

  dbController.find(collections.receiver, {
      email
    })
    .then(result => {
      var res = [];
      for (var i = 0; i < result.length; i++) {
        let item = {
          remindName: result[i].remindName,
          walletNumber: result[i].walletNumber
        }
        res.push(item);
      }
      d.resolve(res);
    })
    .catch(error => {
      console.log(`error when finding receivers: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })

  return d.promise;
}

const updateRemindName = (email, walletNumber, remindName) => {
  const d = q.defer();
  dbController.update(collections.receiver, {
      email,
      walletNumber
    }, {
      remindName
    })
    .then(result => {
      getReceiversOfUser(email).then(res => {
        d.resolve({
          data: res,
          message: `Update remind name successfully!`
        })
      })
    })
    .catch(error => {
      console.log(`error when update remind name ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })
  return d.promise;
}

const deleteReceiver = (email, walletNumber) => {
  const d = q.defer();

  dbController.delete(collections.receiver, {
      email,
      walletNumber
    })
    .then(result => {
      getReceiversOfUser(email)
        .then(res => {
          d.resolve({
            data: res,
            message: `Delete receiver record successfully!`
          })
        })
    })
    .catch(error => {
      console.log(`error when delete receiver ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })
  return d.promise;
}

module.exports = {
  createReceiver,
  getReceiversOfUser,
  updateRemindName,
  deleteReceiver
}