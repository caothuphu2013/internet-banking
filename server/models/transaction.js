const q = require('q');
const ObjectId = require('mongodb').ObjectID;
const {
  collections
} = require('../configs/db');
const {
  dbController
} = require('../controllers/index');
const {
  getAllWalletsOfUser
} = require('./wallet');
const {
  removeDuplicates
} = require('../helpers/utility')

const getAllTransaction = () => {
  const d = q.defer();

  dbController.find(collections.transaction)
    .then(result => {
      d.resolve(result);
    })
    .catch(error => {
      console.log(`error when finding all transaction: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    });

  return d.promise;
}

const getSendingTransaction = (walletNumber) => {
  const d = q.defer();

  let query = {
    originWalletNumber: walletNumber
  }

  dbController.find(collections.transaction, query)
    .then(result => {
      d.resolve(result);
    })
    .catch(error => {
      console.log(`error when finding sending transaction: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    });

  return d.promise;
}

const getReceivingTransaction = (walletNumber) => {
  const d = q.defer();

  let query = {
    destinationWalletNumber: walletNumber
  }

  dbController.find(collections.transaction, query)
    .then(result => {
      d.resolve(result);
    })
    .catch(error => {
      console.log(`error when finding receiving transaction: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    });

  return d.promise;
}

const getRelevantTransaction = (walletNumber) => {
  const d = q.defer();

  dbController.find(collections.transaction, {
      originWalletNumber: walletNumber
    })
    .then(result => {
      dbController.find(collections.transaction, {
          destinationWalletNumber: walletNumber
        })
        .then(res => {
          var combineRes = [...result, ...res];
          d.resolve(combineRes);
        })
        .catch(error => {
          console.log(`error when finding transaction: ${error}`);
          d.reject({
            status: 500,
            message: `server's error`
          })
        })
    })
    .catch(error => {
      console.log(`error when finding transaction: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })
  return d.promise;
}

const getAllTransactionsByUser = (email) => {
  const d = q.defer();

  getAllWalletsOfUser(email)
    .then(async wallets => {
      var combineRes = [];
      //
      for (const wallet of wallets) {
        let res = await getRelevantTransaction(wallet.walletNumber)
        combineRes.push(...res);
      }
      //
      d.resolve(await removeDuplicates(combineRes, '_id'));
    })
    .catch(error => {
      console.log(`error when finding transaction: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })
  return d.promise;
}

const getTransactionById = (id) => {
  const d = q.defer();

  dbController.find(collections.transaction, {
      _id: ObjectId(id)
    })
    .then(res => {
      d.resolve(res);
    })
    .catch(err => {
      console.log(`error when getting transaction by id: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })
  return d.promise;
}

const createTransaction = (originWalletNumber, destinationWalletNumber, amount, message, chargeFee) => {
  const d = q.defer();

  if (chargeFee < 0)
    chargeFee = -chargeFee;

  let data = {
    originWalletNumber,
    destinationWalletNumber,
    amount,
    message,
    chargeFee,
    when: new Date()
  }

  dbController.insert(collections.transaction, data)
    .then(result => {
      d.resolve(result);
    })
    .catch(error => {
      console.log(`error when inserting new transaction: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      });
    })

  return d.promise;
}

module.exports = {
  getAllTransaction,
  getAllTransactionsByUser,
  getSendingTransaction,
  getReceivingTransaction,
  createTransaction,
  getRelevantTransaction,
  getTransactionById
}