const express = require('express');
const {
  transactionModel
} = require('../models/index');
const fnModel = require('../function/function');
const {
  chargeFee
} = require('../configs/server');
const {
  handleSuccess,
  handleError
} = require('../helpers/request');

const {
  verifyToken
} = require('../helpers/auth');

const {
  createVerification,
  verifyTransaction
} = require('../models/verification');

let transactionRoutes = express.Router();

transactionRoutes.post('/verify', (req, res, next) => {
  var data = req.body;
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === data.email && payload.role === 'customer') {
        verifyTransaction(data.email, data.id, data.OTP)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
});

transactionRoutes.get('/find', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload) {
        transactionModel.getRelevantTransaction(req.query.walletNumber)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

transactionRoutes.get('/findall', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === req.query.email) {
        transactionModel.getAllTransactionsByUser(req.query.email)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

transactionRoutes.get('/get', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload) {
        transactionModel.getTransactionById(req.query.id)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

transactionRoutes.post('/request', (req, res, next) => {
  var data = req.body;
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === data.email && payload.role === 'customer') {
        createVerification(data.email, data.originWalletNumber, data.destinationWalletNumber, data.payBy, data.amount, data.message)
          .then(result => {
            console.log(result)
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

module.exports = transactionRoutes;