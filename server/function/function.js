const q = require('q');
const {
  getWallet,
  updateBalance
} = require('../models/wallet');
const {
  createHistory
} = require('../models/history');
const {
  createTransaction
} = require('../models/transaction');

isEmpty = function (data) {
  for (var key in data) {
    if (data.hasOwnProperty(key))
      return false;
  }
  return true;
}

const transferMoney = (originWalletNumber, destinationWalletNumber, payBy, amount, message, chargeFee) => {
  var d = q.defer();
  var oriTotal = amount;
  var desTotal = amount;
  if (amount <= chargeFee) {
    d.reject({
      status: 400,
      message: 'transfer amount is invalid'
    })
    return d.promise;
  }

  getWallet(originWalletNumber)
    .then(res => {
      if (isEmpty(res)) {
        d.reject({
          status: 400,
          message: 'sender wallet is not available'
        })
        return d.promise;
      }
      var senderWallet = res;
      if (senderWallet.isClosed === 'true') {
        d.reject({
          status: 400,
          message: 'sender wallet is closed'
        })
        return d.promise;
      }
      if (payBy === 'sender') {
        oriTotal = oriTotal * 1.0 + chargeFee * 1.0;
      }
      if (senderWallet.balance < oriTotal) {
        d.reject({
          status: 400,
          message: 'balance is not enough'
        })
        return d.promise;
      }
      getWallet(destinationWalletNumber)
        .then(res => {
          if (isEmpty(res)) {
            d.reject({
              status: 400,
              message: 'receiver wallet is not available'
            })
            return d.promise;
          }
          var receiverWallet = res;
          if (receiverWallet.isClosed === 'true') {
            d.reject({
              status: 400,
              message: 'receiver wallet is closed'
            })
            return d.promise;
          }
          if (payBy === 'receiver') {
            desTotal = amount - chargeFee;
            chargeFee = -1 * chargeFee;
          }

          updateBalance(originWalletNumber, senderWallet.balance * 1.0 - oriTotal * 1.0)
            .catch(error => {
              d.reject({
                status: 500,
                message: error.message
              })
            });
          updateBalance(destinationWalletNumber, receiverWallet.balance * 1.0 + desTotal * 1.0)
            .catch(error => {
              d.reject({
                status: 500,
                message: error.message
              })
            })
          createTransaction(originWalletNumber, destinationWalletNumber, amount, message, chargeFee)
            .then(res => {
              createHistory(originWalletNumber, res._id, senderWallet.balance * 1, senderWallet.balance * 1 - oriTotal * 1)
              createHistory(destinationWalletNumber, res._id, receiverWallet.balance * 1, receiverWallet.balance * 1 + desTotal * 1)
              d.resolve({
                data: res,
                message: `Transfer money successfully!`
              });
            })
            .catch(error => {
              d.reject({
                status: 500,
                message: error.message
              })
            })
        })
        .catch(error => {
          d.reject({
            status: 500,
            message: error.message
          })
        })
    })
    .catch(error => {
      d.reject({
        status: 500,
        message: error.message
      })
    })
  return d.promise;
}

const rechargeWallet = (walletNumber, amount) => {
  const d = q.defer();
  getWallet(walletNumber)
    .then(result => {
      if (isEmpty(result)) {
        d.reject({
          status: 400,
          message: `wallet is unavailable`
        })
        return d.promise;
      }
      if (result.isClosed === 'true') {
        d.reject({
          status: 400,
          message: `wallet is closed`
        })
        return d.promise;
      }
      if (amount <= 0) {
        d.reject({
          status: 400,
          message: `recharge amount is invalid`
        })
        return d.promise;
      }
      updateBalance(walletNumber, result.balance * 1 + amount * 1)
        .catch(error => {
          d.reject({
            status: 500,
            message: error.message
          })
          return d.promise;
        })
      createTransaction('bank', walletNumber, amount, 'Recharge wallet', 0)
        .then(res => {
          createHistory(walletNumber, res._id, result.balance * 1, result.balance * 1 + amount * 1)
          d.resolve({
            data: res,
            message: `Recharge wallet ${walletNumber} successfully!`
          })
        })
        .catch(error => {
          d.reject({
            status: 500,
            message: error.message
          })
          return d.promise;
        })
    })
    .catch(error => {
      d.reject({
        status: 500,
        message: error.message
      })
    })

  return d.promise;
}

module.exports = {
  transferMoney,
  rechargeWallet
}