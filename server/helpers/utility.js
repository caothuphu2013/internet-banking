const q = require('q');
const config = require('../configs/server');
var hogan = require('hogan.js');
var fs = require('fs');
var sendGrid = require('@sendgrid/mail');
sendGrid.setApiKey(config.sendGrid.API_KEY);

//Get file
var template = fs.readFileSync('./helpers/media/template/verify_email.html', 'utf-8');
var compiledTemplate = hogan.compile(template);

const removeDuplicates = (myArr, prop) => {
  for (const item of myArr) {
    if (item._id)
      item._id = item._id.toString();
    if (item.when)
      item.when = item.when.toString();
  }
  return myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  });
}

const foo = () => {
  return {result: 'successfull'};
}

const sendVerifyingEmail = (email, originWalletNumber, destinationWalletNumber, payBy, amount, message, OTP) => {
  const d = q.defer();
  var mail = {
    to: email,
    from: 'noreply@internetbanking.newtech',
    subject: 'Internet banking verification',
    html: compiledTemplate.render({
      email,
      webLink: 'localhost:8000',
      originWalletNumber,
      destinationWalletNumber,
      amount,
      message,
      OTP,
      payBy,
      chargeFee: config.chargeFee
    }),
  }
  sendGrid.send(mail)
    .then(res => {
      d.resolve(`succeeded`);
    })
    .catch(err => {
      console.log('Error when sending confirm email to client');
      d.reject({
        status: 500,
        message: `Server's error`
      })
    })

  return d.promise;
}

module.exports = {
  removeDuplicates,
  sendVerifyingEmail,
  foo
}