const moment = require('moment')

const formatInfoUser = (infoUser) => {
    let completePhone = infoUser.prefix + infoUser.phone;
    return {
        email: infoUser.email,
        password: infoUser.password,
        fullName: infoUser.fullName,
        birthday: infoUser.birthday,
        personalNumber: infoUser.personalNumber,
        phone: completePhone,
        address: infoUser.address,
        role: 'customer'
    }
}

const formatInfoStaff = (infoUser) => {
    let completePhone = infoUser.prefix + infoUser.phone;
    return {
        email: infoUser.email,
        password: infoUser.password,
        fullName: infoUser.fullName,
        birthday: infoUser.birthday,
        personalNumber: infoUser.personalNumber,
        phone: completePhone,
        address: infoUser.address,
        role: 'staff'
    }
}

module.exports = {
    formatInfoUser,
    formatInfoStaff
}