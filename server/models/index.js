const userModel = require('./user');
const walletModel = require('./wallet');
const transactionModel = require('./transaction');
const receiverModel = require('./receiver');
const historyModel = require('./history');

module.exports = {
    userModel,
    walletModel,
    transactionModel,
    receiverModel,
    historyModel
}