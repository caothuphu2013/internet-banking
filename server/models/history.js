const q = require('q');
const {
  collections
} = require('../configs/db');
const {
  dbController
} = require('../controllers/index');

const getHistory = (walletNumber) => {
  const d = q.defer();

  dbController.find(collections.history, {
      walletNumber
    })
    .then(result => {
      d.resolve(result);
    })
    .catch(error => {
      console.log(`error when finding history: ${error}`);
      d.reject({
        status: 500,
        message: `server's error`
      })
    })

  return d.promise;
}

const createHistory = (walletNumber, transactionId, previousBalance, currentBalance) => {
  dbController.insert(collections.history, {
      walletNumber,
      transactionId,
      previousBalance,
      currentBalance,
      date: new Date()
    })
    .catch(error => {
      throw `error when creating a history: ${error}`;
    })
}

module.exports = {
  getHistory,
  createHistory
}