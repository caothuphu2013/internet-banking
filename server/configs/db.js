module.exports = {
    url: "mongodb://localhost:27017",
    name: "Banking",
    collections: {
        wallet: "Wallet",
        user: "User",
        transaction: "Transaction",
        token: "Token",
        receiver: "Receiver",
        history: "History",
        verification: "Verification"
    }
}