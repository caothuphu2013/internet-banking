const express = require('express');
const {
  receiverModel
} = require('../models/index');
const {
  handleSuccess,
  handleError
} = require('../helpers/request');

const {
  verifyToken
} = require('../helpers/auth');

let receiverRoutes = express.Router();

receiverRoutes.post('/create', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === req.body.email && payload.role === 'customer') {
        receiverModel.createReceiver(req.body.email, req.body.receiverWalletNumber, req.body.remindName)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

receiverRoutes.get('/', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === req.query.email && payload.role === 'customer') {
        receiverModel.getReceiversOfUser(req.query.email)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

receiverRoutes.post('/update', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload  && payload.email === req.body.email && payload.role === 'customer') {
        receiverModel.updateRemindName(req.body.email, req.body.walletNumber, req.body.remindName)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

receiverRoutes.post('/delete', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];
  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.email === req.body.email && payload.role === 'customer') {
        receiverModel.deleteReceiver(req.body.email, req.body.walletNumber)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

module.exports = receiverRoutes;