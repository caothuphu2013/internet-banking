const express = require('express');
const {
  historyModel
} = require('../models/index');
const {
  handleSuccess,
  handleError
} = require('../helpers/request');

const {
  verifyToken
} = require('../helpers/auth');

let historyRoutes = express.Router();

historyRoutes.get('/', (req, res, next) => {
  const accessToken = req.headers['x_accesstoken'];

  verifyToken(accessToken)
    .then(payload => {
      if (payload && payload.role === 'customer') {
        historyModel.getHistory(req.query.walletNumber)
          .then(result => {
            handleSuccess(res, 200, result);
          })
          .catch(error => {
            handleError(res, error.status, error.message);
          })
      } else {
        handleError(res, 402, "AccessToken is not valid");
      }
    })
    .catch(err => {
      throw err;
    })
})

module.exports = historyRoutes;