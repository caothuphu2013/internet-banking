const q = require('q');
const ObjectId = require('mongodb').ObjectID;
const {
  collections
} = require('../configs/db');
const {
  dbController
} = require('../controllers/index');
const {
  transferMoney
} = require('../function/function');
const config = require('../configs/server');
const {
  sendVerifyingEmail,
  foo
} = require('../helpers/utility');
const {getWallet} = require('../models/wallet');

isEmpty = function (data) {
  for (var key in data) {
    if (data.hasOwnProperty(key))
      return false;
  }
  return true;
}

const createVerification = (email, originWalletNumber, destinationWalletNumber, payBy, amount, message) => {
  const d = q.defer();

  var number = Math.random().toString(16).substring(2, 8);
  var chargeFee = config.chargeFee;
  dbController.find(collections.wallet, {
      email,
      walletNumber: originWalletNumber
    })
    .then(result => {
      if (result.length > 0) {
        var oriTotal = amount;
        var desTotal = amount;
        if (amount <= chargeFee) {
          d.reject({
            status: 400,
            message: 'transfer amount is invalid'
          })
          return d.promise;
        }
        var senderWallet = result[0];
        if (senderWallet.isClosed === 'true') {
          d.reject({
            status: 400,
            message: 'sender\'s wallet is closed'
          })
          return d.promise;
        }
        if (payBy === 'sender') {
          oriTotal = oriTotal * 1.0 + chargeFee * 1.0;
        }
        if (senderWallet.balance < oriTotal) {
          d.reject({
            status: 400,
            message: 'balance is not enough'
          })
          return d.promise;
        }
        getWallet(destinationWalletNumber)
          .then(res => {
            if (isEmpty(res)) {
              d.reject({
                status: 400,
                message: 'receiver wallet is not available'
              })
              return d.promise;
            }
            var receiverWallet = res;
            if (receiverWallet.isClosed === 'true') {
              d.reject({
                status: 400,
                message: 'receiver wallet is closed'
              })
              return d.promise;
            }
            if (payBy === 'receiver') {
              desTotal = amount - chargeFee;
              chargeFee = -1 * chargeFee;
            }
            dbController.insert(collections.verification, {
                email,
                OTP: number,
                originWalletNumber,
                destinationWalletNumber,
                amount,
                payBy,
                message,
                isDone: 'false'
              })
              .then(res => {
                sendVerifyingEmail(email, originWalletNumber, destinationWalletNumber, payBy, amount, message, number)
                  .then(result => {
                    d.resolve({
                      id: res._id,
                      message: `Verification email is sent!`
                    })
                    return d.promise;
                  })
              })
              .catch(error => {
                console.log(error)
                throw `error when insert new verification record: ${error}`
              })
          })
      } else {
        d.reject({
          status: 400,
          message: 'sender wallet is not available'
        })
        return d.promise;
      }
    })
    .catch(error => {
      d.reject({
        status: 500,
        message: error.message
      })
    })
  return d.promise;
}

const verifyTransaction = (email, id, OTP) => {
  const d = q.defer();

  dbController.find(collections.verification, {
      _id: ObjectId(id),
      email
    })
    .then(result => {
      if (result.length > 0) {
        if (result[0].isDone === 'true') {
          d.reject({
            status: 400,
            message: `Transaction had been processed`
          })
          return d.promise;
        }
        if (result[0].OTP === OTP) {
          transferMoney(result[0].originWalletNumber, result[0].destinationWalletNumber, result[0].payBy, result[0].amount, result[0].message, config.chargeFee)
            .then(res => {
              if (res.data) {
                dbController.update(collections.verification, {
                  _id: ObjectId(id)
                }, {
                  isDone: 'true'
                })
                d.resolve({
                  data: res.data,
                  message: `Transaction verified!`
                })
                return d.promise;
              }
            })
            .catch(err => {
              d.reject(err);
            })
        } else {
          d.reject({
            status: 400,
            message: `Verify failed`
          })
        }
      } else {
        d.reject({
          status: 400,
          message: `Can't find transaction to verify`
        })
      }
    })

  return d.promise;
}

module.exports = {
  createVerification,
  verifyTransaction
}