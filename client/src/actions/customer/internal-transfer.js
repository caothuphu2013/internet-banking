import {
    FETCH_USER_WALLET,
    FETCH_USER_WALLET_SUCCESS,
    FETCH_USER_WALLET_FAIL,
    FETCH_USER_WALLETS,
    FETCH_USER_WALLETS_SUCCESS,
    FETCH_USER_WALLETS_FAIL,
    FETCH_RECIPIENTS,
    FETCH_RECIPIENTS_SUCCESS,
    FETCH_RECIPIENTS_FAIL,
    RESET_STORE,
    SEND_TRANSFER_INFORMATION,
    SEND_TRANSFER_INFORMATION_SUCCESS,
    SEND_TRANSFER_INFORMATION_FAIL,
    VERIFY_TRANSACTION,
    VERIFY_TRANSACTION_SUCCESS,
    VERIFY_TRANSACTION_FAIL,
    TOGGLE_MODAL_TRANSFER,
    TRACK_RECIPIENT,
    TRACK_RECIPIENT_SUCCESS,
    TRACK_RECIPIENT_FAIL,
} from '../../constants/customer/internal-transfer';
import { URL_SERVER } from '../../configs/server';

const fetchUserWallets = (email, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_USER_WALLETS });

        fetch(`${URL_SERVER}/wallet/find?email=${email}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_USER_WALLETS_SUCCESS,
                    userWallets: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_USER_WALLETS_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const fetchUserWallet = (walletNumber, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_USER_WALLET });

        fetch(`${URL_SERVER}/wallet/get?walletNumber=${walletNumber}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_USER_WALLET_SUCCESS,
                    paymentAccounts: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_USER_WALLET_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const fetchRecipients = (email, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_RECIPIENTS });

        fetch(`${URL_SERVER}/receiver?email=${email}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_RECIPIENTS_SUCCESS,
                    recipients: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_RECIPIENTS_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const trackRecipient = (walletNumber, accessToken) => {
    return (dispatch) => {
        dispatch({
            type: TRACK_RECIPIENT
        });

        fetch(`${URL_SERVER}/wallet/track?walletNumber=${walletNumber}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: TRACK_RECIPIENT_SUCCESS,
                    emailRecipient: res.data.email,
                    fullNameRecipient: res.data.fullName
                })
            }
            else {
                dispatch({
                    type: TRACK_RECIPIENT_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch (error => {
            console.log(error);
        })
    }
}

const sendTransferInformation = (email, originWalletNumber, destinationWalletNumber, payBy, amount, message, accessToken) => {
    return (dispatch) => {
        dispatch({
            type: SEND_TRANSFER_INFORMATION
        });

        fetch(`${URL_SERVER}/transaction/request`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                x_accesstoken: accessToken
            }),
            body: JSON.stringify({
                email,
                originWalletNumber,
                destinationWalletNumber,
                payBy,
                amount,
                message
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: SEND_TRANSFER_INFORMATION_SUCCESS,
                    idTransaction: res.data.id,
                    messageSuccess: res.data.message
                });
            }
            else {
                dispatch({
                    type: SEND_TRANSFER_INFORMATION_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch (error => {
            console.log(error);
        })
    }
}

const verifyTransaction = (email, id, OTP, accessToken) => {
    return (dispatch) => {
        dispatch({
            type: VERIFY_TRANSACTION
        });

        fetch(`${URL_SERVER}/transaction/verify`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                x_accesstoken: accessToken
            }),
            body: JSON.stringify({
                email,
                id,
                OTP
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: VERIFY_TRANSACTION_SUCCESS,
                    messageSuccess: res.data.message
                });
            }
            else {
                dispatch({
                    type: VERIFY_TRANSACTION_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch (error => {
            console.log(error);
        })
    }
}

const toggleModalTransfer = (isShowModalTransfer) => {
    return (dispatch) => {
        dispatch({
            type: TOGGLE_MODAL_TRANSFER,
            isShowModalTransfer
        });
    }
}

const resetStore = () => {
    return (dispatch) => {
        dispatch({
            type: RESET_STORE
        })
    }
}

export {
    fetchUserWallets,
    fetchUserWallet,
    fetchRecipients,
    sendTransferInformation,
    verifyTransaction,
    toggleModalTransfer,
    trackRecipient,
    resetStore
}