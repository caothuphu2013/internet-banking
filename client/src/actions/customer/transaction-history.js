import {
    FETCH_TRANSACTION_HISTORY,
    FETCH_TRANSACTION_HISTORY_SUCCESS,
    FETCH_TRANSACTION_HISTORY_FAIL,
    RESET_STORE
} from '../../constants/customer/transaction-history';
import { URL_SERVER } from '../../configs/server';


const fetchTransactionHistory = (email, accessToken) => {
    return (dispatch) => {
        dispatch({ type: FETCH_TRANSACTION_HISTORY });

        fetch(`${URL_SERVER}/transaction/findall?email=${email}`, {
            headers: {
                x_accesstoken: accessToken
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.status === 200) {
                dispatch({
                    type: FETCH_TRANSACTION_HISTORY_SUCCESS,
                    transactionHistory: res.data
                });
            }
            else {
                dispatch({
                    type: FETCH_TRANSACTION_HISTORY_FAIL,
                    messageError: res.messageError
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}

const resetStore = () => {
    return dispatch => {
        dispatch({
            type: RESET_STORE
        })
    }
}

export {
    fetchTransactionHistory,
    resetStore
}