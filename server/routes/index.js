const walletRoutes = require('./wallet');
const userRoutes = require('./user');
const transactionRoutes = require('./transaction');
const receiverRoutes = require('./receiver');
const historyRoutes = require('./history');

module.exports = {
    walletRoutes,
    userRoutes,
    transactionRoutes,
    receiverRoutes,
    historyRoutes
}