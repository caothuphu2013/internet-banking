const q = require('q');
const {
	collections
} = require('../configs/db');
const {
	dbController
} = require('../controllers/index');
const {
	maxDeletableBalance
} = require('../configs/server');

const createWallet = (email) => {
	const d = q.defer();
	dbController.find(collections.user, {
			email
		})
		.then(result => {
			if (result.length <= 0) {
				d.reject({
					status: 400,
					message: `user ${email} don't exist`
				})
				return d.promise;
			}
			dbController.find(collections.wallet)
				.then(res => {
					var number = Math.random().toString().substring(3, 13);
					if (res.length > 0) {
						while (true) {
							let count = 0;
							for (var i = 0; i < res.length; i++) {
								if (number === res[i].walletNumber) {
									count++;
									break;
								}
							}
							if (count === 0) break;
							number = Math.random().toString().substring(3, 13);
						}
					}
					dbController.insert(collections.wallet, {
							email,
							walletNumber: number,
							balance: 0,
							creationDate: new Date(),
							isClosed: 'false'
						})
						.then(res => {
							d.resolve({
								data: res,
								message: `Create wallet successfully!`
							});
						})
						.catch(error => {
							d.reject({
								status: 500,
								message: `server's error`
							})
							throw error;
						})
				})
				.catch(error => {
					d.reject({
						status: 500,
						message: `server's error`
					})
					throw error;
				})
		})
		.catch(error => {
			console.log(`error when creating wallet: ${error}`)
			d.reject({
				status: 500,
				message: `server's error`
			})
		})
	return d.promise;
}

const closeWallet = (email, walletNumber) => {
	const d = q.defer();

	dbController.find(collections.wallet, {
			email,
			isClosed: 'false'
		})
		.then(result => {
			if (result.length <= 1) {
				d.reject({
					status: 400,
					message: `user has to have at least 1 open wallet`
				})
				return d.promise;
			}
			dbController.find(collections.wallet, {
					walletNumber
				})
				.then(result => {
					if (result.length > 0) {
						if (result[0].balance > maxDeletableBalance) {
							d.reject({
								status: 400,
								message: `wallet's balance is not suitable for closing, please transfer your remain money to other wallet (a wallet needs to have less than VND ${maxDeletableBalance} to be closed)`
							})
							return d.promise;
						} else {
							dbController.update(collections.wallet, {
									walletNumber
								}, {
									isClosed: 'true'
								})
								.then(result => {
									if (result) {
										getAllWalletsBalanceOfUser(email).then(res => {
											d.resolve({
												data: res,
												message: `wallet ${walletNumber} is closed`
											})
										})
									}
								})
								.catch(error => {
									d.reject({
										status: 500,
										message: `server's error`
									})
									throw error;
								})
						}
					} else {
						d.reject({
							status: 400,
							message: `wallet ${walletNumber} does not exist`
						})
					}
				})
				.catch(error => {
					console.log(`error when closing wallet: ${error}`)
					d.reject({
						status: 500,
						message: `server's error`
					})
				})
		})

	return d.promise;
}

const getAllWalletsOfUser = (email) => {
	const d = q.defer();

	dbController.find(collections.wallet, {
			email
		})
		.then(result => {
			d.resolve(result);
		})
		.catch(error => {
			console.log(`error when finding wallets: ${error}`)
			d.reject({
				status: 500,
				message: `server's error`
			});
		});

	return d.promise;
}

const getWallet = (walletNumber) => {
	const d = q.defer();

	dbController.find(collections.wallet, {
			walletNumber
		})
		.then(result => {
			if (result.length > 0)
				d.resolve(result[0]);
			else d.resolve(null);
		})
		.catch(error => {
			console.log(`error when finding wallet: ${error}`)
			d.reject({
				status: 500,
				message: `server's error`
			});
		});

	return d.promise;
}

const getAllWalletsBalanceOfUser = (email) => {
	const d = q.defer();
	dbController.find(collections.wallet, {
			email: email,
			isClosed: 'false'
		})
		.then(result => {
			if (result.length > 0) {
				var res = [];
				result.forEach(element => {
					let wal = {
						walletNumber: element.walletNumber,
						balance: element.balance,
						creationDate: element.creationDate
					}

					res.push(wal);
				});
				d.resolve(res);
			} else d.resolve(result);
		})
		.catch(error => {
			console.log(`error when finding wallets data: ${error}`)
			d.reject({
				status: 500,
				message: `server's error`
			});
		});

	return d.promise;
}

const getWalletBalance = (walletNumber) => {
	const d = q.defer();

	dbController.find(collections.wallet, {
			walletNumber,
			isClosed: 'false'
		})
		.then(result => {
			if (result.length > 0) {
				var res = {
					walletNumber: result[0].walletNumber,
					balance: result[0].balance,
					creationDate: result[0].creationDate
				};
				d.resolve(res);
			} else d.resolve({});
		})
		.catch(error => {
			console.log(`error when finding wallet data: ${error}`)
			d.reject({
				status: 500,
				message: `server's error`
			});
		});

	return d.promise;
}

const getUserInfoByWallet = (walletNumber) => {
	const d = q.defer();

	dbController.find(collections.wallet, {
			walletNumber
		})
		.then(result => {
			if (result.length > 0) {
				dbController.find(collections.user, {
					email: result[0].email
				}).then(res => {
					if (res.length > 0) {
						d.resolve({
							email: res[0].email,
							fullName: res[0].fullName
						})
					} else {
						d.reject({
							status: 400,
							message: `user not found`
						})
					}
				})
			} else {
				d.reject({
					status: 400,
					message: `wallet not found`
				})
			}
		})

	return d.promise;
}

const updateBalance = (walletNumber, balance) => {
	const d = q.defer();

	dbController.update(collections.wallet, {
			walletNumber
		}, {
			balance
		})
		.then(result => {
			d.resolve(result);
		})
		.catch(error => {
			console.log(`error when updating balance: ${error}`)
			d.reject({
				status: 500,
				message: `server's error`
			});
		});

	return d.promise;
}

module.exports = {
	getAllWalletsOfUser,
	getWallet,
	getAllWalletsBalanceOfUser,
	getWalletBalance,
	updateBalance,
	createWallet,
	closeWallet,
	getUserInfoByWallet
}