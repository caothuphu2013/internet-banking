const express = require('express');
const {
    userModel
} = require('../models/index');
const {
    handleSuccess,
    handleError
} = require('../helpers/request');

const {
    checkAccessToken,
    generateToken
} = require('../helpers/jwt');

const {
    verifyToken
} = require('../helpers/auth');

const jwt = require('../configs/jwt');

let userRoutes = express.Router();

userRoutes.get('/user', (req, res, next) => {
    const accessToken = req.headers['x_accesstoken'];
    verifyToken(accessToken)
        .then(payload => {
            if (payload && payload.role === 'staff') {
                userModel.getUser(req.query.email)
                    .then(result => {
                        handleSuccess(res, 200, result);
                    })
                    .catch(error => {
                        handleError(res, error.status, error.message);
                    })
            } else {
                handleError(res, 402, "AccessToken is not valid");
            }
        })
        .catch(err => {
            throw err;
        })
});

userRoutes.post('/login', (req, res, next) => {
    userModel.login(req.body)
        .then(result => {
            handleSuccess(res, 200, result);
        })
        .catch(error => {
            handleError(res, error.status, error.message);
        })
});

userRoutes.post('/register', (req, res, next) => {
    const accessToken = req.headers['x_accesstoken'];
    verifyToken(accessToken)
        .then(payload => {
            if (payload && payload.role === 'staff') {
                userModel.register(req.body)
                    .then(result => {
                        handleSuccess(res, 200, result);
                    })
                    .catch(error => {
                        handleError(res, error.status, error.message);
                    });
            } else {
                handleError(res, 402, "AccessToken is not valid");
            }
        })
        .catch(err => {
            throw err;
        })
})

userRoutes.post('/registerstaff', (req, res, next) => {
    userModel.registerStaff(req.body)
        .then(result => {
            handleSuccess(res, 200, result);
        })
        .catch(error => {
            handleError(res, error.status, error.message);
        });
})

userRoutes.get('/user/me', (req, res, next) => {
    const accessToken = req.headers['x_accesstoken'];

    checkAccessToken(accessToken)
        .then((user) => {
            handleSuccess(res, 200, user);
        })
        .catch(error => {
            handleError(res, error.status, error.message);
        });
})

userRoutes.get('/token', (req, res, next) => {
    const refreshToken = req.body.refreshToken;
    const username = req.body.username;

    generateToken({
            username
        }, jwt.secret.accessToken, jwt.expires.accessToken)
        .then(result => {
            if (result) {
                userModel.modifiedToken(refreshToken, result)
                    .then(response => {
                        console.log(response.value.accessToken)
                        handleSuccess(res, 200, response.value.accessToken);
                    })
                    .catch(err => {
                        handleError(res, 404, "Invalid Request")
                    })
            } else {
                handleError(res, 404, "Cannot create access token");
            }
        })
        .catch(err => {
            throw err;
        })
})

module.exports = userRoutes;