const q = require('q');
const {
    collections
} = require('../configs/db');
const {
    dbController
} = require('../controllers/index');
const configJWT = require('../configs/jwt');
const {
    comparePassword,
    cryptPassword,
    generateToken
} = require('../helpers/auth');
const {
    formatInfoUser,
    formatInfoStaff
} = require('../helpers/user');

const register = (info) => {
    const d = q.defer();
    let current_user = {};

    const formatedInfo = formatInfoUser(info);

    dbController.find(collections.user, {
            email: formatedInfo.email
        })
        .then(user => {
            if (user.length) {
                d.reject({
                    status: 500,
                    message: "This email already existed"
                });
            } else {
                current_user.email = formatedInfo.email;
                cryptPassword(formatedInfo.password)
                    .then(result => {
                        current_user = {
                            ...formatedInfo,
                            password: result
                        };
                        return dbController.insert(collections.user, current_user);
                    })
                    .then(result => {
                        d.resolve(result);
                    })
                    .catch(error => {
                        d.reject({
                            status: 500,
                            message: "Can not insert account into database"
                        });
                    })
            }
        })
        .catch(error => {
            d.reject({
                status: 500,
                message: "Account can not be found"
            });
        });

    return d.promise;
}

const registerStaff = (info) => {
    const d = q.defer();
    let current_user = {};

    const formatedInfo = formatInfoStaff(info);

    dbController.find(collections.user, {
            email: formatedInfo.email
        })
        .then(user => {
            if (user.length) {
                d.reject({
                    status: 500,
                    message: "This email already existed"
                });
            } else {
                current_user.email = formatedInfo.email;
                cryptPassword(formatedInfo.password)
                    .then(result => {
                        current_user = {
                            ...formatedInfo,
                            password: result
                        };
                        return dbController.insert(collections.user, current_user);
                    })
                    .then(result => {
                        d.resolve(result);
                    })
                    .catch(error => {
                        d.reject({
                            status: 500,
                            message: "Can not insert account into database"
                        });
                    })
            }
        })
        .catch(error => {
            d.reject({
                status: 500,
                message: "Account can not be found"
            });
        });

    return d.promise;
}

const login = (info) => {
    const d = q.defer();
    const {
        email,
        password
    } = info;
    var role = '';

    dbController.find(collections.user, {
            email
        })
        .then(result => {
            if (result.length) {
                role = result[0].role;
                return comparePassword(password, result[0].password)
            }
            return false;
        })
        .then(match => {
            if (match) {
                let accessTokenPromise = generateToken({
                    email,
                    role
                }, configJWT.secret.accessToken, configJWT.expires.accessToken);
                let refreshTokenPromise = generateToken({
                    email,
                    role
                }, configJWT.secret.refreshToken, configJWT.expires.refreshToken);

                q.all([accessTokenPromise, refreshTokenPromise])
                    .then(([accessToken, refreshToken]) => {
                        const da = q.defer();
                        dbController.insert(collections.token, {
                                accessToken,
                                refreshToken
                            })
                            .then(res => {
                                da.resolve({
                                    ...res,
                                    email,
                                    role
                                })
                            })
                        return da.promise;
                    })
                    .then(result => {
                        result ? d.resolve(result) : d.reject({
                            status: 500,
                            message: 'Invalid Token'
                        });
                    })
                    .catch(err => {
                        d.reject({
                            status: 500,
                            message: 'Invalid Token'
                        });
                    })
            } else {
                d.reject({
                    status: 500,
                    message: 'Email or password wrong'
                });
            }
        })
        .catch(error => {
            d.reject({
                status: 500,
                message: 'Account can not be found'
            });
        });

    return d.promise;
}

const getUser = (email) => {
    var d = q.defer();
    dbController.find(collections.user, {
            email
        })
        .then(data => {
            if (data.length > 0) {
                var res = {
                    email: data[0].email,
                    fullName: data[0].fullName,
                    birthday: data[0].birthday,
                    phone: data[0].phone,
                }
                d.resolve(res);
            } else d.resolve(data);
        })
        .catch(error => {
            d.reject({
                status: 500,
                message: "Account can not be found"
            });
        });
    return d.promise;
}

const modifiedToken = (refreshToken, accessToken) => {
    const d = Promise.defer();

    dbController.update(collections.token,{refreshToken}, {accessToken})
    .then(result => {
        d.resolve(result);
    })
    .catch(error => {
        console.log(`error when modifiedToken: ${error}`)
        d.reject({
            status: 500,
            message: `server's error`
        })
    })
    return d.promise;
}

module.exports = {
    register,
    registerStaff,
    login,
    getUser,
    modifiedToken
}